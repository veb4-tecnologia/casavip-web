(function(){
  'use strict';
  angular.module('casavip').constant('angularMomentConfig', {
    timezone: 'America/Sao_Paulo'
  });
})();