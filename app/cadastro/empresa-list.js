(function(){
  'use strict';
  function EmpresaListController(apiService, modalConfirmacao){
    var vm = this;
    vm.empresas = [];

    vm.excluir = function excluir(empresa){
      modalConfirmacao.abrir('Empresas', 'Deseja realmente excluir a empresa <u>' + empresa.razaoSocial + '</u>?<br/><br/><b>ATENÇÃO: TODOS os usuários, clientes, obras/imóveis e visitas desta empresa também serão apagados.</b>')
        .then(function(){
          return modalConfirmacao.abrir('Empresas', 'Atenção! Esta exclusão é de alto risco. <br/><br/>Deseja mesmo continuar e excluir <b>DEFINITIVAMENTE</b> a empresa <u>'+empresa.razaoSocial+'</u>?')
        })
        .then(function(){
          return apiService.cadastro.excluirEmpresa(empresa.codigo);
        })
        .then(function(){
          modalAlerta.abrir('Empresas', 'Empresa excluída com sucesso!');
        })
        .catch(function(err){
          if (err){
            modalAlerta.abrir('Empresas', 'Falha ao excluir empresa: ' + err);
          }
        })
        .finally(load);
    };

    function load(){
      apiService.cadastro.listarEmpresas()
      .then(function(empresas){
        vm.empresas = empresas;
      });
    }

    load();
  }

  EmpresaListController.$inject = ['apiService', 'modalConfirmacao'];

  angular.module('casavip').controller('EmpresaListController', EmpresaListController);
})();