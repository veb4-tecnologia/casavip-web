(function(){
  'use strict';
  function ClienteFormController(apiService, storage, $stateParams, $q, $state, modalAlerta, auth){
    var vm = this;
    vm.edicao = (typeof $stateParams.codigo === 'integer' || /[0-9]/.test($stateParams.codigo));
    vm.titulo = $stateParams.codigo ? 'Edição' : 'Cadastro';
    vm.cliente = {
      endereco: {},
      tiposCliente: []
    };
    vm.tiposClienteSelecionados = [];
    vm.tiposCliente = [];
    vm.podeInserirCliente = auth.possuiPermissao('cli-ins');
    vm.podeEditarCliente = auth.possuiPermissao('cli-edt');
    vm.podeSalvarCliente = vm.edicao ? vm.podeEditarCliente : vm.podeInserirCliente;
    function load(){
      vm.carregando = true;
      vm.tiposCliente = [];
      var tasks = [];
      tasks.push(apiService.cadastro.listarTiposCliente(storage.codEmpresa));
      if (vm.edicao){
        tasks.push(apiService.cadastro.consultarCliente(storage.codEmpresa, $stateParams.codigo));
      }
      $q.all(tasks)
        .then(function success(resultados){
          vm.tiposCliente = resultados[0];
          if (resultados.length > 1){
            vm.cliente = resultados[1];
            vm.tiposClienteSelecionados = vm.cliente.tiposCliente.map(function(item){ return item.codigo; });
          }
        })
        .finally(function(){
          vm.carregando = false;
        });
    }

    vm.selecionarTipoCliente = function selecionarTipoCliente(tipoCliente){
      var idx = vm.tiposClienteSelecionados.indexOf(tipoCliente.codigo);
      if (idx === -1){
        vm.tiposClienteSelecionados.push(tipoCliente.codigo);
      }
      else {
        vm.tiposClienteSelecionados.splice(idx, 1);
      }
    };

    vm.salvarCliente = function salvarCliente(){

      if (!vm.cliente.nome || vm.cliente.nome === ''){
        modalAlerta.abrir('Validação', 'Informe o nome do cliente');
        return false;
      }

      vm.carregando = true;
      vm.cliente.tiposCliente = vm.tiposClienteSelecionados;
      apiService.cadastro.salvarCliente(storage.codEmpresa, vm.cliente)
        .then(function(){
          modalAlerta.abrir('Clientes', 'Cliente gravado com sucesso!')
            .then(function(){
              $state.go('clienteList');
            });
        })
        .finally(function(){
          vm.carregando = false;
        });
    };

    vm.abrirNovoCadastro = function(){
      $state.go('clienteForm', {}, { reload: true, inherit: false, notify: true });
    };

    load();
  }

  ClienteFormController.$inject = ['apiService', 'storage', '$stateParams', '$q', '$state', 'modalAlerta', 'auth'];

  angular.module('casavip').controller('ClienteFormController', ClienteFormController);

})();
