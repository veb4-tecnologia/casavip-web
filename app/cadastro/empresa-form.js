(function(){
  'use strict';
  function EmpresaFormController(apiService, storage, $stateParams, $q, $state, modalAlerta, _empresa){
    var vm = this;
    vm.edicao = (typeof $stateParams.codigo === 'integer' || /[0-9]/.test($stateParams.codigo));
    vm.titulo = $stateParams.codigo ? 'Edição' : 'Cadastro';
    vm.empresa = _empresa || {};
    vm.tiposEmpresaSelecionados = [];
    vm.tiposEmpresa = [];
    function load(){
      vm.carregando = true;
      vm.tiposEmpresa = [];
      var tasks = [];
      if (vm.edicao){
        tasks.push(apiService.cadastro.consultarEmpresa(storage.codEmpresa, $stateParams.codigo));
      }
      $q.all(tasks)
      .then(function success(resultados){
        vm.tiposEmpresa = resultados[0];
        if (resultados.length > 1){
          vm.empresa = resultados[1];
        }
      })
      .finally(function(){
        vm.carregando = false;
      });
    }

    function validar(){
      var msg = [];
      if (!vm.empresa.razaoSocial || vm.empresa.razaoSocial === ''){
        msg.push('Informe o nome da empresa');
      }
      if (!vm.empresa.nomeFantasia || vm.empresa.nomeFantasia === ''){
        msg.push('Informe a razão social da empresa');
      }
      if (msg.length){
        modalAlerta.abrir('Validação', msg.join('\n'));
        return false;
      }
      return true;
    }

    vm.salvarEmpresa = function salvarEmpresa(){

      if (!validar()){
        return false;
      }
      vm.carregando = true;
      apiService.cadastro.salvarEmpresa(vm.empresa)
      .then(function(result){
        if (vm.edicao){
          modalAlerta.abrir('Empresas', 'Empresa atualizada com sucesso!')
            .then(function(){
              $state.go('empresaList');
            });
        }
        else {
          modalAlerta.abrir('Empresas',
            'Empresa gravada com sucesso!<br/>' +
            'Código: ' + result.data.empresa.codigo + '<br/>' +
            'Usuário: ' + result.data.usuario.login + '<br/>' +
            'Senha: ' + result.data.usuario.senha + '<br/><br/>' +
            '<b style="color: red">ATENÇÃO: Anote este usuário e senha, pois não será possível visualizar essas informações depois que esta janela for fechada!</b>'
          )
          .then(function(){
            $state.go('empresaList');
          });
        }
      })
      .catch(function(err){
        modalAlerta.abrir('Empresas', 'A empresa ' + err.data.codigo + ' (' + err.data.razaoSocial + ') já está cadastrada com o CNPJ informado (' + err.data.cnpj + ')');
      })
      .finally(function(){
        vm.carregando = false;
      });
    };

    load();
  }

  EmpresaFormController.$inject = ['apiService', 'storage', '$stateParams', '$q', '$state', 'modalAlerta', '_empresa'];

  angular.module('casavip').controller('EmpresaFormController', EmpresaFormController);

})();
