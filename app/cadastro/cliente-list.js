(function(){
  'use strict';
  function ClienteListController(apiService, storage, modalAlerta, modalConfirmacao, auth){
    var vm = this;
    vm.clientes = [];
    vm.pesquisando = false;
    vm.filtro = { nome: '' };
    vm.podeInserirCliente = auth.possuiPermissao('cli-ins');
    vm.podeExcluirCliente = auth.possuiPermissao('cli-exc');
    vm.formatarTiposCliente = function formatarTiposCliente(cliente){
      var tiposCliente = [];
      for (var i=0; i<cliente.tiposCliente.length; i++){
        var descricao = cliente.tiposCliente[i].descricao;
        if (descricao && descricao !== ''){
          tiposCliente.push(descricao);
        }
      }
      if (tiposCliente.length){
        return tiposCliente.join(', ');
      }
      return '';
    };

    vm.load = function load(){
      vm.pesquisando = true;
      vm.clientes = [];
      apiService.cadastro.listarClientes(storage.codEmpresa, vm.filtro.nome)
        .then(function(clientes){
          vm.clientes = clientes;
        })
        .finally(function(){
          vm.pesquisando = false;
        })
    };

    vm.limparFiltros = function(){
      vm.filtro.nome = '';
      vm.load();
    };

    vm.excluirCliente = function(cliente){
      modalConfirmacao.abrir('Excluir Cliente',
        'Você tem certeza que deseja a excluir o cliente ' + cliente.codigo + '-' + cliente.nome + '?' +
        '<br/><br/><b>ATENÇÃO: TODOS OS IMÓVEIS E AS VISITAS VINCULADAS A ESTE CLIENTE SERÃO EXCLUÍDAS. ESTA AÇÃO É IRREVERSÍVEL!</b>')
      .then(function(){
        return apiService.cadastro.excluirCliente(storage.codEmpresa, cliente.codigo);
      })
      .then(function(){
        modalAlerta.abrir('Excluir cliente', 'Cliente excluído com sucesso!');
        vm.load();
      })
    };

    vm.load();
  }

  ClienteListController.$inject = ['apiService', 'storage', 'modalAlerta', 'modalConfirmacao', 'auth'];

  angular.module('casavip').controller('ClienteListController', ClienteListController);
})();