(function(){
  'use strict';
  angular.module('casavip', ['ui.bootstrap', 'ngStorage', 'ui.router', 'ui.mask', 'angularMoment', 'ngSanitize', 'angular.chips', 'infinite-scroll']);
  angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 500);
})();