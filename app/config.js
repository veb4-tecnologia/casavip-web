(function(){
  function AppConfig($stateProvider, $urlRouterProvider){

    $stateProvider
      .state('principal', {
        url: '/',
        templateUrl: '/app/principal/index.html',
        autenticar: true
      })
      .state('clienteList', {
        url: '/cliente',
        templateUrl: '/app/cadastro/cliente-list.html',
        controller: 'ClienteListController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'cli-lst': {
            parametros: []
          }
        }
      })
      .state('clienteForm', {
        url: '/cliente/form/{codigo}',
        templateUrl: '/app/cadastro/cliente-form.html',
        controller: 'ClienteFormController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'cli-edt': {
            parametros: ['codigo']
          },
          'cli-lst': {
            parametros: ['codigo']
          },
          'cli-ins': {
            parametros: []
          }
        }
      })
      .state('perfilForm', {
        url: '/perfil/form/{codigo}',
        templateUrl: '/app/seguranca/perfil-form.html',
        controller: 'PerfilFormController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'perf-edt': {
            parametros: ['codigo']
          },
          'perf-lst': {
            parametros: ['codigo']
          },
          'perf-ins': {
            parametros: []
          }
        },
        resolve: {
          _perfil: ['storage', '$stateParams', 'apiService', function(storage, $stateParams, apiService){
            if ($stateParams.codigo){
              return apiService.seguranca.consultarPerfil($stateParams.codigo, storage.codEmpresa);
            }
            return null;
          }],
          _modulos: ['storage', 'apiService', function(storage, apiService){
            return apiService.seguranca.listarPermissoes(storage.codEmpresa);
          }]
        }
      })
      .state('perfilList', {
        url: '/perfil',
        templateUrl: '/app/seguranca/perfil-list.html',
        controller: 'PerfilListController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'perf-lst': {
            parametros: []
          }
        },
        resolve: {
          _perfis: ['storage', 'apiService', function(storage, apiService){
            return apiService.seguranca.listarPerfis(storage.codEmpresa)
              .then(function(perfis){
                if (!perfis){
                  return [];
                }
                return perfis.filter(function(item){
                  return !/administrador/i.test(item.nome);
                });
              });
          }]
        }
      })
      .state('empresaList', {
        url: '/empresa',
        templateUrl: '/app/cadastro/empresa-list.html',
        controller: 'EmpresaListController',
        controllerAs: 'vm',
        autenticar: true,
        admin: true
      })
      .state('empresaForm', {
        url: '/empresa/form/{codigo}',
        templateUrl: '/app/cadastro/empresa-form.html',
        controller: 'EmpresaFormController',
        controllerAs: 'vm',
        autenticar: true,
        admin: true,
        resolve: {
          _empresa: ['$stateParams', 'apiService', function($stateParams, apiService){
            if ($stateParams.codigo) {
              return apiService.cadastro.consultarEmpresa($stateParams.codigo);
            }
            return null;
          }]
        }
      })
      .state('imovelForm', {
        url: '/imovel/form/{codigo}',
        templateUrl: '/app/imoveis/imovel-form.html',
        controller: 'ImovelFormController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'imo-edt': {
            parametros: ['codigo']
          },
          'imo-lst': {
            parametros: ['codigo']
          },
          'imo-ins': {
            parametros: []
          }
        },
        resolve: {
          _estados: ['apiService', function(apiService){
            return apiService.cadastro.listarEstados();
          }],
          _imovel: ['storage', '$stateParams', 'apiService', function(storage, $stateParams, apiService){
            if ($stateParams.codigo){
              return apiService.imoveis.consultarImovel(storage.codEmpresa, $stateParams.codigo);
            }
            return null;
          }]
        }
      })
      .state('imovelList', {
        url: '/imovel/list',
        templateUrl: '/app/imoveis/imovel-list.html',
        controller: 'ImovelListController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'imo-lst': {
            parametros: []
          }
        },
        resolve: {
          _usuarios: ['storage', 'apiService', function(storage, apiService){
            return apiService.imoveis.listarUsuarios(storage.codEmpresa);
          }],
          _tiposImovel: ['storage', 'apiService', function(storage, apiService){
            return apiService.imoveis.listarTiposImovel(storage.codEmpresa);
          }],
          _situacoesImovel: ['storage', 'apiService', function(storage, apiService){
            return apiService.imoveis.listarSituacoesImovel(storage.codEmpresa);
          }],
          _estados: ['apiService', function(apiService){
            return apiService.cadastro.listarEstados();
          }]
        }
      })
      .state('usuarioList', {
        url: '/usuarios',
        templateUrl: '/app/seguranca/usuario-list.html',
        controller: 'UsuarioListController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'usu-lst': {
            parametros: []
          }
        },
        resolve: {
          _usuarios: ['storage', 'apiService', function(storage, apiService){
            return apiService.seguranca.listarUsuarios(storage.codEmpresa);
          }]
        }
      })
      .state('usuarioForm', {
        url: '/usuarios/form/{codigo}',
        templateUrl: '/app/seguranca/usuario-form.html',
        controller: 'UsuarioFormController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'imo-edt': {
            parametros: ['codigo']
          },
          'imo-lst': {
            parametros: ['codigo']
          },
          'imo-ins': {
            parametros: []
          }
        },
        resolve: {
          _usuario: ['$stateParams', 'storage', 'apiService', function($stateParams, storage, apiService) {
            if ($stateParams.codigo){
              return apiService.seguranca.consultarUsuario(storage.codEmpresa, $stateParams.codigo);
            }
            return null;
          }],
          _tiposUsuario: ['storage', 'apiService', function(storage, apiService) {
            return apiService.seguranca.listarTiposUsuario(storage.codEmpresa);
          }],
          _perfis: ['storage', 'apiService', function(storage, apiService){
            return apiService.seguranca.listarPerfis(storage.codEmpresa, true);
          }]
        }
      })
      .state('agendamentoVisita', {
        url: '/imovel/{codigo}/agendarVisita',
        templateUrl: '/app/imoveis/agendamento-visita.html',
        controller: 'AgendamentoVisitaController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'vis-ins': {
            parametros: []
          },
          'vis-ins-all': {
            parametros: []
          }
        }
      })
      .state('visitasList', {
        url: '/visitas',
        templateUrl: '/app/vendas/visitas-list.html',
        controller: 'VisitasListController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'vis-lst': {
            parametros: []
          },
          'vis-lst-all': {
            parametros: []
          }
        },
        resolve: {
          _estados: ['apiService', function(apiService){
            return apiService.cadastro.listarEstados();
          }],
          _vendedores: ['storage', 'apiService', 'auth', function(storage, apiService, auth){
            if (auth.possuiPermissao('vis-lst-all')){
              return apiService.seguranca.listarUsuarios(storage.codEmpresa);
            }
            else {
              return apiService.seguranca.listarUsuarios(storage.codEmpresa)
                .then(function(usuarios){
                  if (!usuarios){
                    return [];
                  }
                  return usuarios.filter(function(item){
                    return (item.nome === storage.nomeUsuario);
                  });
                });
            }
          }]
        }
      })
      .state('visitasForm', {
        url: '/visitas/{codigo}',
        templateUrl: '/app/vendas/visitas-form.html',
        controller: 'VisitasFormController',
        controllerAs: 'vm',
        autenticar: true,
        permissoes: {
          'vis-edt': {
            parametros: ['codigo']
          },
          'vis-lst': {
            parametros: ['codigo']
          },
          'vis-lst-all': {
            parametros: ['codigo']
          },
          'vis-ins': {
            parametros: []
          }
        },
        resolve: {
          _visita: ['apiService', 'storage', '$stateParams', function(apiService, storage, $stateParams){
            return apiService.vendas.consultarVisita(storage.codEmpresa, $stateParams.codigo);
          }]
        }
      })
      .state('sair', {
        url: '/logout',
        template: '',
        controller: 'LogoutController',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/login',
        templateUrl: '/app/seguranca/login.html',
        controller: 'LoginController',
        controllerAs: 'vm'
      })
      .state('acesso-negado', {
        url: '/acesso-negado',
        templateUrl: '/app/seguranca/acesso-negado.html'
      });
    $urlRouterProvider.otherwise('/');
  }

  AppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

  function AppRun($rootScope, $state, auth, amMoment){
    $rootScope.autenticado = auth.autenticado();
    amMoment.changeLocale('pt-br');
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
      $rootScope.autenticado = auth.autenticado();
      if (toState.autenticar === true && !auth.autenticado()){
        $state.transitionTo('login');
        event.preventDefault();
      }
      else if (toState.autenticar === true && toState.admin === true && !auth.adminLogado()){
        $state.transitionTo('acesso-negado');
        event.preventDefault()
      }
      else if (toState.autenticar === true && toState.permissoes && !auth.possuiPermissaoAcessarRota(toState, toParams)){
        $state.transitionTo('acesso-negado');
        event.preventDefault();
      }
    })
  }

  AppRun.$inject = ['$rootScope', '$state', 'auth', 'amMoment'];

  angular
    .module('casavip')
    .config(AppConfig)
    .run(AppRun);
})();