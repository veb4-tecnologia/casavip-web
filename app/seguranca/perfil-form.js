(function(){
  'use strict';
  function PerfilFormController($state, _modulos, _perfil, storage, apiService, modalAlerta, auth){
    var vm = this;
    vm.carregando = false;
    vm.edicao = (_perfil !== null && _perfil !== undefined);
    vm.modulos = _modulos;
    vm.perfil = _perfil || {
      codigo: 0,
      permissoes: []
    };
    vm.titulo = (vm.edicao) ? 'Edição' : 'Cadastro';
    vm.podeInserirPerfil = auth.possuiPermissao('perf-ins');
    vm.podeEditarPerfil = auth.possuiPermissao('perf-edt');
    vm.podeSalvar = (vm.edicao) ? vm.podeEditarPerfil : vm.podeInserirPerfil;
    vm.bloquearEdicaoNome = (vm.edicao && /administrador/i.test(vm.perfil.nome));

    function selecionarPermissao(permissao){
      var idx = vm.perfil.permissoes.indexOf(permissao.tag);
      if (idx === -1){
        vm.perfil.permissoes.push(permissao.tag);
      }
      else {
        vm.perfil.permissoes.splice(idx, 1);
      }
    }

    function validar(){
      var msg = [];
      if (!angular.isDefined(vm.perfil.nome) || vm.perfil.nome === ''){
        msg.push('Informe o nome do perfil de segurança');
      }
      else if (!vm.edicao && /administrador/i.test(vm.perfil.nome)){
        msg.push('Já existe um perfil Administrador cadastrado. Você não pode criar outro perfil de Administrador');
      }
      if (msg.length){
        modalAlerta.abrir('Validação', msg.join('<br/>'));
        return false;
      }
      return true;
    }

    function salvar(){
      if (!validar()){
        return;
      }

      vm.carregando = true;
      apiService.seguranca.salvarPerfil(storage.codEmpresa, {
        codigo: vm.perfil.codigo,
        nome: vm.perfil.nome,
        permissoes: vm.perfil.permissoes
      })
      .then(function(){
        return modalAlerta.abrir('Perfil de Segurança', 'Perfil de Segurança gravado com sucesso!');
      })
      .then(function() {
        $state.go('perfilList');
      })
      .catch(function(err) {
        modalAlerta.abrir('Erro', err.message || err);
      })
      .finally(function(){
        vm.carregando = false;
      });
    }

    function abrirNovoCadastro(){
      $state.go('perfilForm', {}, { reload: true, inherit: false, notify: true });
    }

    ////////////////////////////////////////////////////

    vm.selecionarPermissao = selecionarPermissao;
    vm.salvar = salvar;
    vm.abrirNovoCadastro = abrirNovoCadastro;
  }
  PerfilFormController.$inject = ['$state', '_modulos', '_perfil', 'storage', 'apiService', 'modalAlerta', 'auth'];
  angular.module('casavip').controller('PerfilFormController', PerfilFormController);
})();