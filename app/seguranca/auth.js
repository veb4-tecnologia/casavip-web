(function(){
  'use strict';
  function Auth(storage){

    function autenticado(){
      if (!!storage.token && !!storage.codUsuario && !!storage.permissoes){
        return true;
      }
      return false;
    }

    function possuiPermissao(permissao){
      return (storage.permissoes && storage.permissoes.indexOf(permissao) > -1);
    }

    function adminLogado(){
      return (autenticado() && storage.codEmpresa === 0);
    }

    function possuiPermissaoAcessarRota(toState, toParams){
      if (!toState.permissoes || Object.keys(toState.permissoes).length === 0){
        return true;
      }
      var keys = Object.keys(toState.permissoes);
      var permissoesComParametros = 0;
      for (var i=0; i<keys.length; i++){
        var permissao = toState.permissoes[keys[i]];
        if (permissao.parametros && permissao.parametros.length){
          permissoesComParametros++;
        }
      }
      for (var i=0; i<keys.length; i++){
        var key = keys[i];
        var permissao = toState.permissoes[key];
        if (permissao.parametros && permissao.parametros.length){
          permissoesComParametros--;
        }
        var parametrosInformados = Object.keys(toParams).filter(function(key){ return angular.isDefined(toParams[key]) && toParams[key] !== ''; });
        if (parametrosInformados && parametrosInformados.length > 0){
          var todosParametrosInformados = true;
          for (var j=0; j<permissao.parametros.length; j++){
            var parametroEsperado = permissao.parametros[j];
            if (parametrosInformados.indexOf(parametroEsperado) === -1){
              todosParametrosInformados = false;
              break;
            }
          }
          if (todosParametrosInformados && !possuiPermissao(key) && permissoesComParametros === 0){
            return false;
          }
        }
        if (possuiPermissao(key)){
          return true;
        }
      }
      return false;
    }

    return {
      autenticado: autenticado,
      possuiPermissao: possuiPermissao,
      adminLogado: adminLogado,
      possuiPermissaoAcessarRota: possuiPermissaoAcessarRota
    }
  }

  Auth.$inject = ['storage'];

  angular.module('casavip').factory('auth', Auth);

})();