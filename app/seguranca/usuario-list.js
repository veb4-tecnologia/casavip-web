(function(){
  'use strict';
  function UsuarioListController(storage, apiService, formatacao, modalConfirmacao, modalAlerta, _usuarios, auth){
    var vm = this;
    vm.podeInserirUsuario = auth.possuiPermissao('usu-ins');
    vm.podeEditarUsuario = auth.possuiPermissao('usu-edt');
    vm.podeExcluirUsuario = auth.possuiPermissao('usu-exc');
    function recarregar(){
      apiService.seguranca.listarUsuarios(storage.codEmpresa)
        .then(function(usuarios) {
          vm.usuarios = usuarios;
        });
    }

    vm.usuarios = _usuarios;
    if (vm.usuarios){
      for (var i=0; i<vm.usuarios.length; i++){
        vm.usuarios[i].tipos = vm.usuarios[i].tiposUsuario.map(function(item) {
          return item.descricao;
        }).join(', ');
      }
    }

    vm.excluirUsuario = function(usuario) {
      modalConfirmacao.abrir('Excluir usuário', 'Deseja realmente excluir o usuário ' + usuario.nome + '(' + usuario.login + ') ?')
        .then(function(){
          return apiService.seguranca.excluirUsuario(storage.codEmpresa, usuario.codigo);
        })
        .then(function(){
          recarregar();
          modalAlerta.abrir('Usuários', 'Usuário foi excluído com sucesso!');
        })
        .catch(function(err) {
          modalAlerta.abrir('Erro', err.message);
        });
    }

  }

  UsuarioListController.$inject = ['storage', 'apiService', 'formatacao', 'modalConfirmacao', 'modalAlerta', '_usuarios', 'auth'];

  angular.module('casavip').controller('UsuarioListController', UsuarioListController);

})();