(function(){
  function UsuarioFormController(_tiposUsuario, _usuario, _perfis, storage, apiService, modalAlerta, $state, $q, auth){
    var vm = this;
    vm.tiposUsuario = _tiposUsuario;
    vm.carregando = false;
    vm.edicao = (_usuario !== null && _usuario !== undefined);
    vm.titulo = (vm.edicao) ? 'Edição' : 'Cadastro';
    vm.alterarSenha = !vm.edicao;
    vm.usuario = _usuario || {
      tiposUsuario: [],
      ativo: true
    };
    vm.perfis = _perfis;
    vm.tiposUsuarioSelecionados = (_usuario) ? _usuario.tiposUsuario.map(function(item){ return item.codigo; }) : [];
    vm.selecionarTipoUsuario = function selecionarTipoUsuario(tipoUsuario){
      var idx = vm.tiposUsuarioSelecionados.indexOf(tipoUsuario.codigo);
      if (idx === -1){
        vm.tiposUsuarioSelecionados.push(tipoUsuario.codigo);
      }
      else {
        vm.tiposUsuarioSelecionados.splice(idx, 1);
      }
    };
    vm.podeInserirUsuario = auth.possuiPermissao('usu-ins');
    vm.podeEditarUsuario = auth.possuiPermissao('usu-edt');
    vm.podeSalvar = (vm.edicao) ? vm.podeEditarUsuario : vm.podeInserirUsuario;
    function validar(){
      var dfd = $q.defer();

      var msg = [];
      if (!vm.usuario.nome || vm.usuario.nome.toString().trim() === ''){
        msg.push('Informe o Nome');
      }
      if (!vm.usuario.email || vm.usuario.email.toString().trim() === ''){
        msg.push('Informe o E-mail');
      }
      if (!vm.usuario.login || vm.usuario.login.toString().trim() === ''){
        msg.push('Informe o Login');
      }
      if (vm.alterarSenha){
        if (!vm.usuario.senha || vm.usuario.senha.toString().trim() === ''){
          msg.push('Informe a Senha')
        }
        if (!vm.confirmacaoSenha || vm.confirmacaoSenha.toString().trim() === ''){
          msg.push('Informe a Confirmação de Senha');
        }
        if (vm.confirmacaoSenha !== vm.usuario.senha){
          msg.push('Senha e Confirmação devem ser idênticas');
        }
      }
      if (angular.isUndefined(vm.usuario.perfil) || vm.usuario.perfil === 0){
        msg.push('Selecione o Perfil de Segurança');
      }
      if (msg.length){
        modalAlerta.abrir('Validação', msg.join('<br/>'));
        dfd.reject();
      }

      $q.all([
        apiService.seguranca.listarUsuarios(storage.codEmpresa, { login: vm.usuario.login }),
        apiService.seguranca.listarUsuarios(storage.codEmpresa, { email: vm.usuario.email })
      ]).then(function(results) {
        var usuariosLogin = results[0];
        var loginValido;
        if (vm.edicao){
          loginValido = (!usuariosLogin || usuariosLogin.length === 0 || usuariosLogin[0].codigo === vm.usuario.codigo);
        }
        else {
          loginValido = (!usuariosLogin || usuariosLogin.length === 0);
        }
        if (!loginValido){
          msg.push('Login já está sendo utilizado por outro usuário.');
        }

        var usuariosEmail = results[1];
        var emailValido;
        if (vm.edicao){
          emailValido = (!usuariosEmail || usuariosEmail.length === 0 || usuariosEmail[0].codigo === vm.usuario.codigo);
        }
        else {
          emailValido = (!usuariosEmail || usuariosEmail.length === 0);
        }
        if (!emailValido){
          msg.push('E-mail já está sendo utilizado por outro usuário.');
        }
      })
      .finally(function(){
        if (msg.length){
          dfd.reject(msg.join('<br/>'));
        }
        dfd.resolve();
      });
      return dfd.promise;
    }

    vm.habilitarAlteracaoSenha = function() {
      vm.alterarSenha = true;
    };

    vm.salvar = function(){
      vm.carregando = true;
      validar()
        .then(function() {
          vm.usuario.tiposUsuario = vm.tiposUsuarioSelecionados;
          return apiService.seguranca.salvarUsuario(storage.codEmpresa, vm.usuario)
        })
        .then(function() {
          return modalAlerta.abrir('Usuário', 'Usuário gravado com sucesso!');
        })
        .then(function() {
          $state.go('usuarioList');
        })
        .catch(function(err) {
          modalAlerta.abrir('Erro', err.message || err);
        })
        .finally(function(){
          vm.carregando = false;
        });
    };

    vm.abrirNovoCadastro = function (){
      $state.go('usuarioForm', {}, { reload: true, inherit: false, notify: true });
    };
  }

  UsuarioFormController.$inject = ['_tiposUsuario', '_usuario', '_perfis', 'storage', 'apiService', 'modalAlerta', '$state', '$q', 'auth'];

  angular.module('casavip').controller('UsuarioFormController', UsuarioFormController);

})();