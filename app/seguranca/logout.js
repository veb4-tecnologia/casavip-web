(function(){
  function LogoutController(storage, $state, $rootScope, $window){
    delete storage.codEmpresa;
    delete storage.empresa;
    delete storage.token;
    delete storage.nomeUsuario;
    delete storage.permissoes;
    delete storage.codUsuario;
    $rootScope.autenticado = false;
    $state.go('login');
  }

  LogoutController.$inject = ['storage', '$state', '$rootScope', '$window'];

  angular.module('casavip').controller('LogoutController', LogoutController);
})();