(function(){
  'use strict';
  function LoginController(apiService, storage, $state, modalAlerta){
    var vm = this;
    vm.$storage = storage;
    vm.login = '';
    vm.empresa = null;
    vm.autenticando = false;
    vm.textoBtnEntrar = 'ENTRAR';
    vm.entrar = function(){
      vm.textoBtnEntrar = 'AUTENTICANDO...';
      vm.autenticando = true;
      apiService.autenticar(vm.login, vm.senha, vm.empresa)
        .then(function success(data){
          vm.$storage.token = data.token;
          vm.$storage.empresa = data.empresaVinculada;
          vm.$storage.codEmpresa = vm.empresa;
          vm.$storage.codUsuario = data.usuario.codigo;
          vm.$storage.nomeUsuario = data.usuario.nome;
          vm.$storage.permissoes = data.usuario.permissoes;
          $state.transitionTo('principal');
        })
        .catch(function(){
          modalAlerta.abrir('Autenticação', 'Usuário ou senha inválidos');
        })
        .finally(function(){
          vm.textoBtnEntrar = 'ENTRAR';
          vm.autenticando = false;
        });
    };
  }

  LoginController.$inject = ['apiService', 'storage', '$state', 'modalAlerta'];

  angular.module('casavip').controller('LoginController', LoginController);
})();
