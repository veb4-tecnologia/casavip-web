(function(){
  'use strict';
  function PerfilListController(storage, apiService, formatacao, modalConfirmacao, modalAlerta, _perfis, auth){
    var vm = this;

    function recarregar(){
      apiService.seguranca.listarPerfis(storage.codEmpresa)
      .then(function(perfis) {
        if (!perfis){
          vm.perfis = [];
        }
        else {
          vm.perfis = perfis.filter(function(item){
            return !/administrador/i.test(item.nome);
          });
        }
      });
    }

    vm.perfis = _perfis || [];
    vm.podeInserirPerfil = auth.possuiPermissao('perf-ins');
    vm.podeEditarPerfil = auth.possuiPermissao('perf-edt');
    vm.podeExcluirPerfil = auth.possuiPermissao('perf-exc');

    vm.excluirPerfil = function(perfil) {
      modalConfirmacao.abrir('Excluir perfil', 'Deseja realmente excluir o Perfil de Segurança ' + perfil.nome + '?')
      .then(function(){
        return apiService.seguranca.excluirPerfil(perfil.codigo, storage.codEmpresa);
      })
      .then(function(){
        recarregar();
        modalAlerta.abrir('Perfis de Segurança', 'Perfil de Segurança foi excluído com sucesso!');
      })
      .catch(function(err) {
        modalAlerta.abrir('Erro', err.message);
      });
    }

  }

  PerfilListController.$inject = ['storage', 'apiService', 'formatacao', 'modalConfirmacao', 'modalAlerta', '_perfis', 'auth'];
  angular.module('casavip').controller('PerfilListController', PerfilListController);

})();