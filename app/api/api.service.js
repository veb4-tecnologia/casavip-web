(function(){
  'use strict';
  function ApiService($http, $q, base64, storage, moment){

    //var baseUrl = 'https://acasavip-api.herokuapp.com';
    var baseUrl = 'https://casavip-api-dev.herokuapp.com';
    //var baseUrl = 'http://127.0.0.1:3000';
    //var baseUrl = 'http://localhost:3000';
    var bearerToken;
    function getBearerToken(){
      return storage.token;
    }

    function get(path, codEmpresa, params){
      var dfd = $q.defer();
      var url = undefined;
      if (codEmpresa !== undefined && codEmpresa !== null){
        url = baseUrl + '/empresas/' + codEmpresa + path;
      }
      else {
        url = baseUrl + path;
      }
      $http({
        method: 'GET',
        url: url,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        params: params
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function delet(path, codEmpresa){
      var dfd = $q.defer();
      var url = undefined;
      if (codEmpresa !== undefined && codEmpresa !== null){
        url = baseUrl + '/empresas/' + codEmpresa + path;
      }
      else {
        url = baseUrl + path;
      }
      $http({
        method: 'DELETE',
        url: url,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function put(path, data, codEmpresa){
      var dfd = $q.defer();
      var url = undefined;
      if (codEmpresa !== undefined && codEmpresa !== null){
        url = baseUrl + '/empresas/' + codEmpresa + path;
      }
      else {
        url = baseUrl + path;
      }
      $http({
        method: 'PUT',
        url: url,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        data: data
      }).then(function success(){
        dfd.resolve();
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function post(path, data, codEmpresa){
      var dfd = $q.defer();
      var url = undefined;
      if (codEmpresa !== undefined && codEmpresa !== null){
        url = baseUrl + '/empresas/' + codEmpresa + path;
      }
      else {
        url = baseUrl + path;
      }
      $http({
        method: 'POST',
        url: url,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        data: data
      }).then(function success(data){
        dfd.resolve(data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function autenticar(usuario, senha, codEmpresa){
      var dfd = $q.defer();
      $http({
        method: 'POST',
        url: baseUrl + '/empresas/' + codEmpresa + '/tokens',
        headers: {
          Authorization: 'Basic ' + base64.encode(usuario + ':' + senha)
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarTiposCliente(codEmpresa){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/tiposCliente',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarTiposImovel(codEmpresa){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/tiposImovel',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarSituacoesImovel(codEmpresa){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/situacoesImovel',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarVendedores(codEmpresa){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/vendedores',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function salvarCliente(codEmpresa, cliente){
      var dfd = $q.defer();
      $http({
        method: cliente.codigo > 0 ? 'PUT' : 'POST',
        url: baseUrl + '/empresas/' + codEmpresa + '/clientes' + (cliente.codigo > 0 ? '/' + cliente.codigo : ''),
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        data: cliente
      }).then(function success(){
        dfd.resolve();
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarClientes(codEmpresa, nome){
      var dfd = $q.defer();
      var options = {
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/clientes',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      };
      if (nome && nome.trim() !== ''){
        options.params = { q: nome };
      }
      $http(options).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function consultarCliente(codEmpresa, codCliente){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/clientes/' + codCliente,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function excluirCliente(codEmpresa, codCliente){
      return delet('/clientes/' + codCliente, codEmpresa);
    }

    function listarImoveis(codEmpresa, filtros, skip, limit){
      var dfd = $q.defer();
      var params = filtros || {};
      params.skip = skip;
      params.limit = limit;
      $http({
        method: 'GET',
        params: params,
        url: baseUrl + '/empresas/' + codEmpresa + '/imoveis',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarImoveisExcel(codEmpresa, filtros){
      var queryString = '';
      var params = [];
      Object.keys(filtros)
      .forEach(function(key){
        var value = filtros[key];
        if (value !== undefined && value !== null && value !== ''){
          params.push(
            key + '=' + encodeURI(filtros[key])
          );
        }
      })
      params.push(
        'token=' + getBearerToken()
      );
      queryString = params.join('&');
      return baseUrl + '/empresas/' + codEmpresa + '/imoveis.xlsx?' + queryString;
    }

    function consultarImovel(codEmpresa, codImovel){
      var dfd = $q.defer();
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/imoveis/' + codImovel,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function salvarImovel(codEmpresa, imovel){
      var dfd = $q.defer();
      $http({
        method: imovel.codigo > 0 ? 'PUT' : 'POST',
        url: baseUrl + '/empresas/' + codEmpresa + '/imoveis' + (imovel.codigo > 0 ? '/' + imovel.codigo : ''),
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        data: imovel
      }).then(function success(){
        dfd.resolve();
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function excluirImovel(codEmpresa, codImovel){
      return delet('/imoveis/' + codImovel, codEmpresa);
    }

    function agendarVisita(codEmpresa, visita){
      var dfd = $q.defer();
      $http({
        method: visita.codigo > 0 ? 'PUT' : 'POST',
        url: baseUrl + '/empresas/' + codEmpresa + '/imoveis/' + visita.imovel.codigo + '/visitas',
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        },
        data: visita
      }).then(function success(result){
        dfd.resolve(result.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarVisitas(codEmpresa, filtros){
      var dfd = $q.defer();
      var timezone = 'America/Sao_Paulo';
      var params = JSON.parse(JSON.stringify(filtros));
      params.todosUsuarios = true;
      if (filtros.cliente){
        params.cliente = filtros.cliente;
      }
      if (filtros.dataInicial){
        params.dataInicial = moment.tz(filtros.dataInicial, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataFinal){
        params.dataFinal = moment.tz(filtros.dataFinal, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataInicialCheckin){
        params.dataInicialCheckin = moment.tz(filtros.dataInicialCheckin, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataFinalCheckin){
        params.dataFinalCheckin = moment.tz(filtros.dataFinalCheckin, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataInicialConclusaoVisita){
        params.dataInicialConclusaoVisita = moment.tz(filtros.dataInicialConclusaoVisita, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataFinalConclusaoVisita){
        params.dataFinalConclusaoVisita = moment.tz(filtros.dataFinalConclusaoVisita, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataInicialCadastro){
        params.dataInicialCadastro = moment.tz(filtros.dataInicialCadastro, timezone).format('YYYY-MM-DD');
      }
      if (filtros.dataFinalCadastro){
        params.dataFinalCadastro = moment.tz(filtros.dataFinalCadastro, timezone).format('YYYY-MM-DD');
      }
      if (filtros.endereco){
        params.endereco = filtros.endereco;
      }
      if (filtros.bairro){
        params.bairro = filtros.bairro;
      }
      if (filtros.cidade){
        params.cidade = filtros.cidade;
      }
      if (filtros.uf){
        params.uf = filtros.uf;
      }
      if (filtros.situacao){
        params.situacao = filtros.situacao;
      }
      $http({
        method: 'GET',
        url: baseUrl + '/empresas/' + codEmpresa + '/visitas',
        params: params,
        headers: {
          Authorization: 'Bearer ' + getBearerToken()
        }
      }).then(function success(response){
        dfd.resolve(response.data);
      }, function error(err){
        dfd.reject(err);
      });
      return dfd.promise;
    }

    function listarUsuarios(codEmpresa, params){
      return get('/usuarios', codEmpresa, params);
    }

    function consultarVisita(codEmpresa, codVisita){
      return get('/visita/' + codVisita, codEmpresa);
    }

    function excluirVisita(codEmpresa, codVisita){
      return delet('/visitas/' + codVisita, codEmpresa);
    }

    function excluirUsuario(codEmpresa, codUsuario){
      return delet('/usuarios/' + codUsuario, codEmpresa);
    }

    function listarTiposUsuario(codEmpresa){
      return get('/tiposUsuario', codEmpresa);
    }

    function salvarUsuario(codEmpresa, usuario){
      if (usuario.codigo > 0){
        return put('/usuario/' + usuario.codigo, usuario, codEmpresa);
      }
      return post('/usuarios', usuario, codEmpresa);
    }

    function consultarUsuario(codEmpresa, codUsuario){
      return get('/usuario/' + codUsuario, codEmpresa);
    }

    function checkInVisita(codEmpresa, codVisita){
      return post('/visitas/' + codVisita + '/checkin', null, codEmpresa);
    }

    function salvarVisita(codEmpresa, visita){
      return put('/visitas/' + visita.codigo, visita, codEmpresa);
    }

    function listarEmpresas(){
      return get('/empresas');
    }

    function salvarEmpresa(empresa){
      if (empresa.codigo > 0){
        return put('/empresas/' + empresa.codigo, empresa);
      }
      return post('/empresas', empresa);
    }

    function listarDescricaoTiposCliente(){
      return get('/tiposCliente');
    }

    function listarDescricaoTiposImovel(){
      return get('/tiposImovel');
    }

    function listarDescricaoTiposUsuario(){
      return get('/tiposUsuario');
    }

    function listarDescricaoSituacoesImovel(){
      return get('/situacoesImovel');
    }

    function excluirEmpresa(codEmpresa){
      return delet('/empresas/' + codEmpresa);
    }

    function consultarEmpresa(codEmpresa) {
      return get('/empresas/' + codEmpresa);
    }

    function listarPermissoes(codEmpresa){
      return get('/empresas/' + codEmpresa + '/permissoes');
    }

    function listarPerfis(codEmpresa, somenteCabecalho){
      var params;
      if (somenteCabecalho === true){
        params = {
          header: 'true'
        };
      }
      return get('/perfis', codEmpresa, params);
    }

    function consultarPerfil(codPerfil, codEmpresa){
      return get('/perfis/' + codPerfil, codEmpresa);
    }

    function excluirPerfil(codPerfil, codEmpresa){
      return delet('/perfis/' + codPerfil, codEmpresa);
    }

    function salvarPerfil(codEmpresa, perfil){
      if (perfil.codigo > 0){
        return put('/perfis/' + perfil.codigo, perfil, codEmpresa);
      }
      return post('/perfis', perfil, codEmpresa);
    }

    function listarEstados(){
      var dfd = $q.defer();
      dfd.resolve([
        'AC',
        'AL',
        'AM',
        'AP',
        'BA',
        'CE',
        'DF',
        'ES',
        'GO',
        'MA',
        'MT',
        'MS',
        'MG',
        'PA',
        'PB',
        'PR',
        'PE',
        'PI',
        'RJ',
        'RN',
        'RS',
        'RO',
        'RR',
        'SC',
        'SP',
        'SE',
        'TO'
      ]);
      return dfd.promise;
    }

    function listarUsuariosImoveis(codEmpresa){
      return get('/imoveis/usuarios', codEmpresa);
    }

    return {
      autenticar: autenticar,
      cadastro: {
        listarTiposCliente: listarTiposCliente,
        salvarCliente: salvarCliente,
        listarClientes: listarClientes,
        consultarCliente: consultarCliente,
        excluirCliente: excluirCliente,
        listarVendedores: listarVendedores,
        listarEmpresas: listarEmpresas,
        listarDescricaoTiposCliente: listarDescricaoTiposCliente,
        listarDescricaoTiposImovel: listarDescricaoTiposImovel,
        listarDescricaoTiposUsuario: listarDescricaoTiposUsuario,
        listarDescricaoSituacoesImovel: listarDescricaoSituacoesImovel,
        salvarEmpresa: salvarEmpresa,
        excluirEmpresa: excluirEmpresa,
        consultarEmpresa: consultarEmpresa,
        listarEstados: listarEstados
      },
      imoveis: {
        listarTiposImovel: listarTiposImovel,
        listarSituacoesImovel: listarSituacoesImovel,
        salvarImovel: salvarImovel,
        listarImoveis: listarImoveis,
        consultarImovel: consultarImovel,
        agendarVisita: agendarVisita,
        excluirImovel: excluirImovel,
        listarUsuarios: listarUsuariosImoveis,
        listarImoveisExcel: listarImoveisExcel
      },
      vendas: {
        listarVisitas: listarVisitas,
        consultarVisita: consultarVisita,
        checkin: checkInVisita,
        salvarVisita: salvarVisita,
        excluirVisita: excluirVisita
      },
      seguranca: {
        listarUsuarios: listarUsuarios,
        excluirUsuario: excluirUsuario,
        listarTiposUsuario: listarTiposUsuario,
        salvarUsuario: salvarUsuario,
        consultarUsuario: consultarUsuario,
        listarPermissoes: listarPermissoes,
        consultarPerfil: consultarPerfil,
        listarPerfis: listarPerfis,
        salvarPerfil: salvarPerfil,
        excluirPerfil: excluirPerfil
      },
      getBearerToken: getBearerToken
    };
  }

  ApiService.$inject = ['$http', '$q', 'base64', 'storage', 'moment'];

  angular.module('casavip').factory('apiService', ApiService);

})();
