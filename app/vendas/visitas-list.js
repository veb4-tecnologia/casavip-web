(function(){
  'use strict';
  function VisitasListController(formatacao, apiService, storage, _vendedores, modalAlerta, modalConfirmacao, $window, $state, $filter, auth, _estados){
    var dates = ['dataInicial', 'dataFinal', 'dataInicialCheckin', 'dataFinalCheckin', 'dataInicialConclusaoVisita', 'dataFinalConclusaoVisita', 'dataInicialCadastro', 'dataFinalCadastro'];
    var vm = this;
    vm.vendedores = _vendedores;
    vm.filtro = {};
    vm.estados = _estados;
    vm.codUsuario = parseInt(storage.codUsuario, 10);
    vm.podeListarVisitasTodosUsuarios = auth.possuiPermissao('vis-lst-all');
    vm.podeEditarVisitas = auth.possuiPermissao('vis-edt');
    vm.podeEditarVisitasTodosUsuarios = auth.possuiPermissao('vis-edt-all');
    vm.podeExcluirPropriasVisitas = auth.possuiPermissao('vis-exc');
    vm.podeExcluirVisitasTodosUsuarios = auth.possuiPermissao('vis-exc-all');
    vm.getTextoEdicaoVisita = function(visita){
      var textoEditar = 'Visualizar';
      if (visita.vendedor.codigo !== vm.codUsuario && vm.podeEditarVisitasTodosUsuarios){
        textoEditar = 'Editar';
      }
      else if (visita.vendedor.codigo == vm.codUsuario && vm.podeEditarVisitas){
        textoEditar = 'Editar';
      }
      return textoEditar;
    };
    vm.podeExcluirVisita = function(visita){
      if (visita.vendedor.codigo !== vm.codUsuario){
        return vm.podeExcluirVisitasTodosUsuarios;
      }
      return vm.podeExcluirPropriasVisitas;
    };
    if (vm.podeListarVisitasTodosUsuarios){
      vm.filtro.vendedor = null;
    }
    else {
      vm.filtro.vendedor = vm.codUsuario;
    }
    function configurarDatas(){
      for (var i=0; i<dates.length; i++) {
        var date = dates[i];
        vm.filtro[date] = new Date();
        vm.filtro[date].setHours(12, 0, 0, 0, 0);
        vm[date] = {
          opened: false,
          dateOptions: {
            showWeeks: false
          }
        }
      }
      vm.filtro.dataInicialCheckin = null;
      vm.filtro.dataFinalCheckin = null;
      vm.filtro.dataInicialConclusaoVisita = null;
      vm.filtro.dataFinalConclusaoVisita = null;
      vm.filtro.dataInicialCadastro = null;
      vm.filtro.dataFinalCadastro = null;
    }
    configurarDatas();
    vm.visitas = [];
    vm.abrirCalendario = function(campo){
      vm[campo].opened = !vm[campo].opened
    };
    vm.formatarEndereco = formatacao.formatarEndereco;
    vm.formatarSituacao = formatacao.formatarSituacaoVisita;
    vm.load = function load(){
      apiService.vendas.listarVisitas(storage.codEmpresa, vm.filtro)
        .then(function success(visitas){
          vm.visitas = visitas;
          storage.ultimoFiltroVisitas = vm.filtro;
        });
    };
    vm.abrirNoMapa = function(){
      var enderecos = [];
      for (var i=0; i<vm.visitas.length; i++){
        var visita = vm.visitas[i];
        enderecos.push(formatacao.formatarEndereco(visita.imovel.endereco));
      }
      var enderecoInicial = enderecos.splice(0, 1);
      if (enderecos.length){
        var enderecoFinal = enderecos.splice(enderecos.length - 1, 1);
        var url = 'https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=' + encodeURI(enderecoInicial)
          + '&destination=' + encodeURI(enderecoFinal)
          + '&waypoints=' + encodeURI(enderecos.join('|'));
        $window.open(url)
      }
    };
    vm.limparFiltros = function limparFiltros(){
      for (var i=0; i<dates.length; i++) {
        var date = dates[i];
        vm.filtro[date] = new Date();
        vm.filtro[date].setHours(12, 0, 0, 0, 0);
      }
      vm.filtro.dataInicialCheckin = null;
      vm.filtro.dataFinalCheckin = null;
      vm.filtro.dataInicialConclusaoVisita = null;
      vm.filtro.dataFinalConclusaoVisita = null;
      vm.filtro.dataInicialCadastro = null;
      vm.filtro.dataFinalCadastro = null;
      if (vm.podeListarVisitasTodosUsuarios){
        vm.filtro.vendedor = null;
      }
      else {
        vm.filtro.vendedor = vm.codUsuario;
      }
      vm.filtro.cliente = null;
      vm.filtro.bairro = null;
      vm.filtro.cidade = null;
      vm.filtro.uf = null;
      vm.filtro.situacao = null;
      vm.load();
    };
    if (storage.ultimoFiltroVisitas) {
      vm.filtro = storage.ultimoFiltroVisitas;
      for (var i=0; i<dates.length; i++) {
        var date = dates[i];
        if (vm.filtro[date] && typeof vm.filtro[date] === 'string'){
          vm.filtro[date] = new Date(vm.filtro[date]);
        }
      }
    }
    vm.abrirVisita = function(visita){
      var link = $state.href('visitasForm', { codigo: visita.codigo });
      $window.open(link);
    };
    vm.excluirVisita = function(visita){
      modalConfirmacao.abrir('Excluir Visita',
        'Você tem certeza que deseja a excluir a visita Nº ' + visita.codigo + ', do cliente ' + visita.imovel.clientes[0].nome + ', agendada para o dia ' + $filter('date')(visita.data, 'dd/MM/yyyy') + '?' +
        '<br/><br/><b>ATENÇÃO: ESTA AÇÃO É IRREVERSÍVEL!</b>')
        .then(function(){
          return apiService.vendas.excluirVisita(storage.codEmpresa, visita.codigo);
        })
        .then(function(){
          modalAlerta.abrir('Excluir visita', 'Visita excluída com sucesso!');
          vm.load();
        })
    };
    vm.load();
  }

  VisitasListController.$inject = ['formatacao', 'apiService', 'storage', '_vendedores', 'modalAlerta', 'modalConfirmacao', '$window', '$state', '$filter', 'auth', '_estados'];

  angular.module('casavip').controller('VisitasListController', VisitasListController);

})();
