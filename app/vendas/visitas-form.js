(function(){
  'use strict';
  function VisitasFormController(storage, apiService, formatacao, moment,  modalConfirmacao, modalAlerta, $state, _visita, $window, auth){
    var vm = this;
    vm.registrandoCheckin = false;
    vm.visita = _visita;
    vm.formatarEndereco = formatacao.formatarEndereco;
    vm.formatarSituacao = formatacao.formatarSituacaoVisita;
    vm.checkinPosicionado = !!(vm.visita && vm.visita.posicaoCheckin && vm.visita.posicaoCheckin.latitude && vm.visita.posicaoCheckin.longitude);
    vm.codUsuario = parseInt(storage.codUsuario, 10);
    vm.visitaPropria = (vm.codUsuario == vm.visita.vendedor.codigo);
    vm.podeEditarVisita = (vm.visitaPropria) ? auth.possuiPermissao('vis-edt') : auth.possuiPermissao('vis-edt-all');
    vm.podeFinalizarVisita = (vm.visitaPropria) ? auth.possuiPermissao('vis-end') : auth.possuiPermissao('vis-end-all');
    vm.podeRegistrarCheckIn = (vm.visitaPropria) ? auth.possuiPermissao('vis-chk') : auth.possuiPermissao('vis-chk-all');
    vm.formatarData = function(data){
      if (!data){
        return '';
      }
      return moment.tz(data, 'America/Sao_Paulo').format('DD/MM/YYYY HH:mm');
    };
    vm.registrarCheckIn = function(){
      modalConfirmacao.abrir('Registrar Check-in', 'Deseja realmente registrar o check-in para esta visita? Esta ação é irreversível.')
        .then(function(){
          vm.registrandoCheckin = true;
          vm.carregando = true;
          apiService.vendas.checkin(storage.codEmpresa, vm.visita.codigo)
          .then(function(){
            return apiService.vendas.consultarVisita(storage.codEmpresa, vm.visita.codigo);
          })
          .then(function(visita){
            vm.visita = visita;
          })
          .finally(function(){
            vm.carregando = false;
            vm.registrandoCheckin = false;
          });
        });
    };
    vm.salvar = function(){
      vm.salvando = true;
      vm.carregando = true;
      apiService.vendas.salvarVisita(storage.codEmpresa, vm.visita)
        .then(function(){
          return apiService.vendas.consultarVisita(storage.codEmpresa, vm.visita.codigo);
        })
        .then(function(visita){
          vm.visita = visita;
        })
        .finally(function(){
          vm.carregando = false;
          vm.salvando = false;
        });
    };
    vm.finalizar = function(){
      var oldStatus = vm.visita.status;
      if (!vm.visita.resultado){
        modalAlerta.abrir('Validação', 'Informe o resultado da visita');
        return;
      }

      modalConfirmacao.abrir('Finalizar Visita', 'Deseja realmente finalizar a visita? Esta ação é irreversível.')
        .then(function(){
          vm.finalizando = true;
          vm.carregando = true;
          vm.visita.dataFim = new Date();
          vm.visita.status = 'concluida';
          apiService.vendas.salvarVisita(storage.codEmpresa, vm.visita)
          .then(function(){
            return apiService.vendas.consultarVisita(storage.codEmpresa, vm.visita.codigo);
          })
          .then(function(visita){
            vm.visita = visita;
          })
          .catch(function(ex){
            vm.visita.dataFim = null;
            vm.visita.status = oldStatus;
            modalAlerta.abrir('Erro', ex.message);
          })
          .finally(function(){
            vm.finalizando = false;
            vm.salvando = false;
          });
        });
    };
    vm.abrirClienteEdicao = function(cliente){
      var link = $state.href('clienteForm', { codigo: cliente.codigo });
      $window.open(link);
    };
    vm.abrirImovelEdicao = function(imovel){
      var link = $state.href('imovelForm', { codigo: imovel.codigo });
      $window.open(link);
    };
  }

  VisitasFormController.$inject = ['storage', 'apiService', 'formatacao', 'moment',  'modalConfirmacao','modalAlerta', '$state', '_visita', '$window', 'auth'];

  angular.module('casavip').controller('VisitasFormController', VisitasFormController);

})();