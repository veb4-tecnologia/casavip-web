(function(){
  'use strict';
  function StatusBarController(storage){
    var vm = this;
    vm.empresa = storage.empresa;
    vm.nomeUsuario = storage.nomeUsuario;
  }

  angular.module('casavip').controller('StatusBarController', ['storage', StatusBarController]);
})();
