(function(){
  'use strict';
  function MenuController(storage, auth, $rootScope){
    var vm = this;
    vm.navegacaoFechada = true;
    vm.podeExibirEmpresaAdmin = (storage.codEmpresa == 0);
    vm.podeListarClientes = false;
    vm.podeListarVisitas = false;
    vm.podeListarUsuarios = false;
    vm.podeListarPerfis = false;
    vm.podeListarImoveis = false;
    $rootScope.$watch('autenticado', function(){
      vm.podeExibirEmpresaAdmin = (storage.codEmpresa == 0);
      vm.podeListarClientes = auth.possuiPermissao('cli-lst');
      vm.podeListarVisitas = auth.possuiPermissao('vis-lst') || auth.possuiPermissao('vis-lst-all');
      vm.podeListarUsuarios = auth.possuiPermissao('usu-lst');
      vm.podeListarPerfis = auth.possuiPermissao('perf-lst');
      vm.podeListarImoveis = auth.possuiPermissao('imo-lst');
    });
  }

  MenuController.$inject = ['storage', 'auth', '$rootScope'];

  angular.module('casavip').controller('MenuController', MenuController);

})();