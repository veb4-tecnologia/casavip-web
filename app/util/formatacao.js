(function(){
  'use strict;';
  function Formatacao(){
    return {
      formatarEndereco: function(endereco){
        var enderecoCompleto = endereco.logradouro;
        if (endereco.numero){
          enderecoCompleto += ', ' + endereco.numero;
        }
        if (endereco.complemento){
          enderecoCompleto += ' - ' + endereco.complemento;
        }
        enderecoCompleto += ' - ' + endereco.bairro;
        enderecoCompleto += ' - ' + endereco.cidade;
        enderecoCompleto += '/' + endereco.uf;
        return enderecoCompleto;
      },
      formatarSituacaoVisita: function(visita){
        var situacao = visita.status || visita;
        switch (situacao){
          case 'concluida':
            return 'Concluída';
          case 'em-andamento':
            return 'Em andamento';
          default:
            return 'Pendente';
        }
      }
    };
  }

  angular.module('casavip').factory('formatacao', Formatacao);
})();