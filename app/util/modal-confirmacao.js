(function(){
  'use strict';
  function ModalConfirmacao($uibModal, $q){
    var modalInstance;
    return {
      abrir: function(titulo, mensagem){
        var dfd = $q.defer();
        modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '/app/util/modal-confirmacao.html',
          controller: function(){
            var vm = this;
            vm.titulo = titulo;
            vm.mensagem = mensagem;
            vm.ok = function(){
              modalInstance.close();
              dfd.resolve();
            };
            vm.cancel = function(){
              modalInstance.close();
              dfd.reject();
            };
          },
          controllerAs: 'vm'
        });
        return dfd.promise;
      }
    }
  }

  ModalConfirmacao.$inject = ['$uibModal', '$q'];

  angular.module('casavip').factory('modalConfirmacao', ModalConfirmacao);

})();