(function(){
  'use strict';
  function ModalAlerta($uibModal, $q){
    var modalInstance;
    return {
      abrir: function(titulo, mensagem){
        var dfd = $q.defer();
        modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '/app/util/modal-alerta.html',
          controller: function(){
            var vm = this;
            vm.titulo = titulo;
            vm.mensagem = mensagem;
            vm.ok = function(){
              modalInstance.close();
              dfd.resolve();
            };
          },
          controllerAs: 'vm'
        });
        return dfd.promise;
      }
    }
  }

  ModalAlerta.$inject = ['$uibModal', '$q'];

  angular.module('casavip').factory('modalAlerta', ModalAlerta);

})();