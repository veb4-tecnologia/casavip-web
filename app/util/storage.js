(function(){
  'use strict';
  function Storage($localStorage){
    return $localStorage;
  }

  Storage.$inject = ['$localStorage'];

  angular.module('casavip').factory('storage', Storage);
})();