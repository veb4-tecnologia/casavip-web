(function(){
  'use strict';
  function ImovelListController(apiService, storage, modalConfirmacao, modalAlerta, auth, _tiposImovel, _situacoesImovel, _estados, _usuarios, $window){
    var dates = ['dataInicialCadastro', 'dataFinalCadastro'];
    var LIMIT = 10;
    var vm = this;
    vm.carregarMais = true;
    vm.filtro = storage.ultimoFiltroImoveis || {};
    vm.carregando = false;
    vm.carregandoMais = false;
    vm.ultimoFiltroCarregado = angular.copy(vm.filtro);
    vm.imoveis = [];
    vm.usuarios = _usuarios;
    vm.estados = _estados;
    vm.tiposImovel = _tiposImovel;
    vm.situacoesImovel = _situacoesImovel;
    vm.podeInserirImovel = auth.possuiPermissao('imo-ins');
    vm.podeEditarImovel = auth.possuiPermissao('imo-edt');
    vm.podeExcluirImovel = auth.possuiPermissao('imo-exc');
    vm.podeAgendarVisita = auth.possuiPermissao('vis-ins') || auth.possuiPermissao('vis-ins-all');
    vm.getFotoPrincipal = function(imovel){
      if (imovel.fotos && imovel.fotos.length){
        return imovel.fotos[0];
      }
      return '/assets/imovel-sem-foto.jpg';
    };

    vm.formatarEndereco = function(endereco){
      var enderecoCompleto = endereco.logradouro;
      if (endereco.numero){
        enderecoCompleto += ', ' + endereco.numero;
      }
      if (endereco.complemento){
        enderecoCompleto += ' - ' + endereco.complemento;
      }
      enderecoCompleto += ' - ' + endereco.bairro;
      enderecoCompleto += ' - ' + endereco.cidade;
      enderecoCompleto += '/' + endereco.uf;
      return enderecoCompleto;
    };

    vm.getLinkGMaps = function(endereco){
      var enderecoCompleto = vm.formatarEndereco(endereco);
      return 'https://maps.google.com/?q=' + enderecoCompleto;
    };

    vm.abrirAgendamentoVisita = function(){

    };

    vm.excluirImovel = function(imovel){
      modalConfirmacao.abrir('Excluir Imóvel',
        'Você tem certeza que deseja o imóvel Nº ' + imovel.codigo + ', vinculado ao cliente ' + imovel.clientes[0].nome + '?' +
        '<br/><br/><b>ATENÇÃO: TODAS AS VISITAS VINCULADAS A ESTE IMÓVEL SERÃO EXCLUÍDAS. ESTA AÇÃO É IRREVERSÍVEL!</b>')
      .then(function(){
        return apiService.imoveis.excluirImovel(storage.codEmpresa, imovel.codigo);
      })
      .then(function(){
        modalAlerta.abrir('Excluir imovel', 'Imovel excluído com sucesso!');
        load();
      })
    };

    vm.limparFiltros = function(){
      vm.filtro.descricao = null;
      vm.filtro.cliente = null;
      vm.filtro.usuario = null;
      vm.filtro.bairro = null;
      vm.filtro.cidade = null;
      vm.filtro.uf = null;
      vm.filtro.tipo = null;
      vm.filtro.situacao = null;
      vm.filtro.dataInicialCadastro = null;
      vm.filtro.dataFinalCadastro = null;
      load();
    };

    function load(){
      vm.carregando = true;
      return apiService.imoveis.listarImoveis(storage.codEmpresa, vm.filtro, 0, LIMIT)
      .then(function(imoveis){
        vm.imoveis = imoveis;
        storage.ultimoFiltroImoveis = vm.filtro;
        vm.ultimoFiltroCarregado = angular.copy(vm.filtro);
      })
      .finally(function(){
        vm.carregando = false;
        vm.carregarMais = true;
      });
    }

    function download(){
      var url = apiService.imoveis.listarImoveisExcel(storage.codEmpresa, vm.filtro);
      if (url){
        $window.location.href = url;
      }
    }
    vm.download = download;

    function configurarDatas(){
      for (var i=0; i<dates.length; i++) {
        var date = dates[i];
        vm.filtro[date] = new Date();
        vm.filtro[date].setHours(12, 0, 0, 0, 0);
        vm[date] = {
          opened: false,
          dateOptions: {
            showWeeks: false
          }
        }
      }
      vm.filtro.dataInicialCadastro = null;
      vm.filtro.dataFinalCadastro = null;
    }
    vm.abrirCalendario = function(campo){
      vm[campo].opened = !vm[campo].opened
    };

    vm.load = load;
    vm.carregarMaisImoveis = function(){
      if (vm.carregandoMais === true || !vm.carregarMais || vm.imoveis.length === 0){
        return;
      }
      var skip = vm.imoveis.length;
      vm.carregandoMais = true;
      apiService.imoveis.listarImoveis(storage.codEmpresa, vm.ultimoFiltroCarregado, skip, LIMIT)
        .then(function(imoveis){
          if (imoveis && imoveis.length){
            for (var i=0; i<imoveis.length; i++){
              vm.imoveis.push(imoveis[i]);
            }
          }
          else {
            vm.carregarMais = false;
          }
        })
        .finally(function(){
          vm.carregandoMais = false;
        });
    };

    configurarDatas();
    load();
  }

  ImovelListController.$inject = ['apiService', 'storage', 'modalConfirmacao', 'modalAlerta', 'auth', '_tiposImovel', '_situacoesImovel', '_estados', '_usuarios', '$window'];

  angular.module('casavip').controller('ImovelListController', ImovelListController);
})();