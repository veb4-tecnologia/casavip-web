(function(){
  'use strict';
  function AgendamentoVisitaController(apiService, storage, $q, $stateParams, $state, auth){
    var vm = this;
    var codUsuario = parseInt(storage.codUsuario, 10);
    vm.podeAgendarVisitaTodosUsuarios = auth.possuiPermissao('vis-ins-all');
    vm.imovel = {
      cliente: {
        nome: ''
      },
      endereco: null
    };
    vm.visita = {
      data: new Date()
    };
    vm.vendedores = [];
    vm.dataVisita = {
      opened: false,
      dateOptions: {
        minDate: new Date(),
        showWeeks: false
      }
    };

    function validar(){
      var msg = [];
      if (!vm.visita.data){
        msg.push('Informe a Data da Visita');
      }
      if (!vm.visita.hora || vm.visita.hora.trim() === ''){
        msg.push('Informe a Hora da Visita');
      }
      else if (!/\d{2}:\d{2}/.test(vm.visita.hora)){
        msg.push('Hora inválida');
      }
      if (!vm.visita.motivo || vm.visita.motivo.trim() === ''){
        msg.push('Informe o motivo');
      }
      if (msg.length){
        alert(msg);
        return false;
      }
      return true;
    }

    vm.abrirCalendarioDataVisita = function(){
      vm.dataVisita.opened = !vm.dataVisita.opened;
    };

    vm.formatarEndereco = function(endereco){
      if (!endereco){
        return '';
      }
      var enderecoCompleto = endereco.logradouro;
      if (endereco.numero){
        enderecoCompleto += ', ' + endereco.numero;
      }
      if (endereco.complemento){
        enderecoCompleto += ' - ' + endereco.complemento;
      }
      enderecoCompleto += ' - ' + endereco.bairro;
      enderecoCompleto += ' - ' + endereco.cidade;
      enderecoCompleto += '/' + endereco.uf;
      return enderecoCompleto;
    };

    vm.salvar = function(){
      if (!validar()){
        return false;
      }

      var data = vm.visita.data;
      var horas = /^(\d{2}):(\d{2})$/.exec(vm.visita.hora);
      data.setHours(horas[1]);
      data.setMinutes(horas[2]);

      var visita = {
        imovel: vm.imovel,
        vendedor: {
          codigo: vm.vendedor.codigo,
          nome: vm.vendedor.nome
        },
        data: data.toISOString(),
        motivo: vm.visita.motivo
      };
      apiService.imoveis.agendarVisita(storage.codEmpresa, visita)
        .then(function(visitaAgendada){
          alert('Visita Nº' + visitaAgendada.codigo + ' agendada com sucesso!');
          $state.go('imovelList');
        })

    };

    function load(){
      $q.all([
        apiService.cadastro.listarVendedores(storage.codEmpresa),
        apiService.imoveis.consultarImovel(storage.codEmpresa, $stateParams.codigo)
      ]).then(function(resultados){
        var vendedores = resultados[0];
        if (vm.podeAgendarVisitaTodosUsuarios){
          vm.vendedores = vendedores;
        }
        else {
          vm.vendedores = vendedores.filter(function(item){
            return item.codigo == codUsuario;
          });
        }
        vm.vendedor = vm.vendedores[0];
        vm.imovel = resultados[1];
        vm.imovel.cliente = vm.imovel.clientes[0];
      });
    }
    load();
  }

  AgendamentoVisitaController.$inject = ['apiService', 'storage', '$q', '$stateParams', '$state', 'auth'];

  angular.module('casavip').controller('AgendamentoVisitaController', AgendamentoVisitaController);

})();