(function(){
  'use strict';
  function ImovelFormController(
    apiService,
    storage,
    $q,
    $state,
    modalAlerta,
    modalConfirmacao,
    _imovel,
    auth,
    _estados
  ){
    var edicao = (!!_imovel);
    var vm = this;
    vm.isEdicao = edicao;
    vm.estados = _estados;
    vm.titulo = (edicao === true) ? 'Edição' : 'Cadastro';
    vm.tiposImovel = [];
    vm.situacoesImovel = [];
    vm.clientes = [];
    vm.imovel = _imovel || {
      endereco: {}
    };
    vm.responsaveis = [];
    vm.responsavel = {
      cliente: null
    };
    vm.podeInserirImovel = auth.possuiPermissao('imo-ins');
    vm.podeEditarImovel = auth.possuiPermissao('imo-edt');
    vm.podeSalvar = (edicao ? vm.podeEditarImovel : vm.podeInserirImovel);

    function validar(){
      var msg = [];
      if (!vm.imovel.descricao || vm.imovel.descricao.trim() === ''){
        msg.push('Informe a Descrição');
      }
      if (!vm.imovel.endereco.logradouro || vm.imovel.endereco.logradouro.trim() === ''){
        msg.push('Informe o Endereço');
      }
      if (!vm.imovel.endereco.bairro || vm.imovel.endereco.bairro.trim() === ''){
        msg.push('Informe o Bairro');
      }
      if (!vm.imovel.endereco.cidade || vm.imovel.endereco.cidade.trim() === ''){
        msg.push('Informe a Cidade');
      }
      if (!vm.imovel.endereco.uf || vm.imovel.endereco.uf.trim() === ''){
        msg.push('Selecione a UF');
      }
      if (msg.length){
        modalAlerta.abrir('Validação', msg.join('<br/>'));
        return false;
      }
      return true;
    }

    function validarResponsavel(){
      var msg = [];
      if (!vm.responsavel.cliente) {
        msg.push('Selecione o cliente');
      }
      else if (vm.responsavel.cliente.tiposCliente && vm.responsavel.cliente.tiposCliente.length && !vm.responsavel.tipoResponsavel) {
          msg.push('Selecione o tipo de responsável');
      }
      if (msg.length) {
        modalAlerta.abrir('Validação', msg.join('<br/>'));
        return false;
      }
      return true;
    }

    function load(){
      $q.all([
        apiService.imoveis.listarTiposImovel(storage.codEmpresa),
        apiService.imoveis.listarSituacoesImovel(storage.codEmpresa),
        apiService.cadastro.listarClientes(storage.codEmpresa)
      ]).then(function(resultados){
        vm.tiposImovel = resultados[0];
        vm.situacoesImovel = resultados[1];
        vm.clientes = resultados[2];
        vm.imovel.cliente = vm.imovel.clientes[0];
        vm.imovel.tipoImovel = vm.tiposImovel[0];
        vm.imovel.situacaoImovel = vm.situacoesImovel[0];

        if (vm.clientes.length > 1) {
          vm.responsaveis = vm.imovel.clientes.slice(1);
        }
      });
    }
    load();

    vm.salvar = function salvar(){
      if (!validar()){
        return;
      }

      var imovel = JSON.parse(JSON.stringify(vm.imovel));
      delete imovel.cliente.endereco;
      imovel.clientes = [imovel.cliente];
      for (var i=0; i<vm.responsaveis.length; i++) {
        var responsavel = vm.responsaveis[i];
        delete responsavel.endereco;
        imovel.clientes.push(responsavel);
      }
      delete imovel.cliente;
      apiService.imoveis.salvarImovel(storage.codEmpresa, imovel)
        .then(function(){
          modalAlerta.abrir('Imóveis', 'Imóvel gravado com sucesso')
            .then(function(){
              $state.go('imovelList');
            });
        });
    };
    vm.inserirResponsavel = function inserirResponsavel() {
      if (!validarResponsavel()) {
        return;
      }
      var cliente = vm.responsavel.cliente;
      cliente.tipoResponsavel = vm.responsavel.tipoResponsavel;
      vm.imovel.clientes.push(cliente);
      vm.responsaveis = vm.imovel.clientes.slice(1);
    };
    vm.excluirResponsavel = function excluirResponsavel(responsavel, index){
      modalConfirmacao.abrir('Responsáveis', 'Deseja mesmo excluir o responsável ' + responsavel.nome + '?')
        .then(function() {
          vm.responsaveis.splice(index, 1);
        });
    };
    vm.abrirNovoCadastro = function(){
      $state.go('imovelForm', {}, { reload: true, inherit: false, notify: true });
    };
  }

  ImovelFormController.$inject = [
    'apiService',
    'storage',
    '$q',
    '$state',
    'modalAlerta',
    'modalConfirmacao',
    '_imovel',
    'auth',
    '_estados'
  ];

  angular.module('casavip').controller('ImovelFormController', ImovelFormController);

})();